# volxtz

An informal voting/polling mechanism for the Tezos blockchain. 

## Mission and Usecase

The initial use case of Volxtz will be for informal, off-chain polling regarding the popularity of various parts of various governance proposals within the Tezos blockchain.

## Volxtz description

Voltxtz is a tool for gathering and verifying informal opinions, polls or votes. It is explicitly unenforcable and meant to allow interested parties to understand how much representation each position has prior to an official governance process taking place. 

It will allow groups and service providers to form questionnaires or polls that users can vote on with a vote weight commensurate with their coin balance. This will be done by connecting a wallet, selecting the poll they want to participate in and signing a message with their vote of 'yay' 'nay' or 'abstain.' Users will then be provided with a reciept signed also by the volxtz hosting instance. Should the database of votes become the subject of malice or incompetence, the vote results up to the time of corruption should be able to be independently reconstructed with the individuals reciepts. 

Volxtz will maintain it's own centralized database where future, past and ongoing polls can be explored by users. There will also be a notification system for specific polls one wishes to sign up for.

Any platform where Volxtz is integrated ( such as the Agora ) could also publish the recipts in realtime, this would provide a another means of poll reconstruction should the user lose their reciept.

Anyone will be able to create a poll on Volxtz. They would connect their wallet, fill out the form and submit it. Their Wallet address and XTZ balance would be published, along with their vote on the proposal. In the fiest iteration there will be spam prevention and admin review on new proposals. 

To prevent hoping addresses and voting with the same XTZ, all vote balances will be based off of a priorly defined snapshot at the time of the vote. On poll creation, there creator will choose from among the optional upcoming snapshots. The specific snapshot block number will be provided with a given polls details. 

Users ( specifically Bakers ) will be able to provision keys onchain. This will allow them to have a Tezos address that votes on behalf of another address for security purposes.  

## Volxtz execution

When a particular measure or measures are proposed to the community, an announcement is sent out publicly and or based on registration with the group or service provider.

At the appropriate place and time, for the appropriate period the poll will be open. Participants will connect their crypto wallet. Select the chosen active measure and their ballot ( yay, nay, abstain ).

Volxtz will use the signing mechanism we are familiar with in crypto wallet functions to prove ownership of a certain balance. Of special note, the signing mechanism is not something to be used lightly. Volxtz will go to great legnths to educate users about this and provide them more secure options should they desire to use them. 

The balance of the signing address will be for a ‘yay’ ‘nay’ or ‘abstain’ vote. All other active balances on the chain will have an implicit ‘non-participation’ vote.

## Future Features

### Custom voting options

The capacity to define the poll options beyond 'yay' 'nay' abstain.' Or perhaps to limit them to only 'yay' or 'nay' etc.. 

### Tezos token voting

In the future, Volxtz will be usable with any token ( including NFTs ) on the Tezos network. 

### NFT Poll reciepts

An NFT minted and provided as proof and reciept of your having voted and as an optional promotion. 

### Volxtz DAO 

Eventually Volxtz will be administrated by a DAO. Once potential usecase for this is having poposals submitted along with a bond. If the DAO decides a given poll is malicious ( entirely based on it's judgement, whether sound or arbitrary ) the bond would be forfiet. 

## FAQ

Q: Will you need to register or provide identifying information?

A: For Participants, there will be no registry or info gathering other than what is explicitly necessary to provide the service. Until a DAO is online, poll creators may have to provide some extra information in order to prevent abuse. 

## Funding

Volxtz is entirely funded through voluntary contribution, donations and ultimately me (Jarrod Woodard) if niether of those things come through. If you want to donate some Tez as motivation and a thanks, you can use this address: tz1MV2hVfMmYURzmtNvrZ5CCG625Bg2qSLAZ 

## Community and Contact

You can chat with interested parties using the [element chat](https://matrix.to/#/#Volxtz:matrix.org)

You can alos follow the discussion on [The Tezos Agora](https://forum.tezosagora.org/t/volxtz-a-tool-for-informal-polling-to-assist-with-consensus-formation-prior-to-proposal-injection/4117)

