# Volxtz - Project State

For more real-time discussion: 

The Element chat: #Volxtz:matrix.org
[The Tezos Agora](https://forum.tezosagora.org/t/volxtz-a-tool-for-informal-polling-to-assist-with-consensus-formation-prior-to-proposal-injection/4117)

## How to Contribution

You can either help out on a volunteer basis or bid on a particular task. All code will be free and open source and unless a better licence is preferred, MIT will be used. 

Do not hesitate to submit a bid on a particular task or tasks, publicly or privately. There is nothing wrong with getting paid for your time / effort and expertise.

## Beta Minimally Viable Product

Specifications:

* Be able to connect wallet to the hosting server
* Must have a database with polls and votes stored
* Be able to create a poll
* Be able to vote on a poll
* Be able to claim key provisioning
* Volxtz must be able to validate provisioned keys
* Be able to provide polling results

Future necessary features to be aware of throughout design:

* The host will have a set of keys for signing new polls and votes
* New polls and votes will be provided a receipt

## In Active Development

* A rudimentary html website - (https://gitlab.com/JarrodWoodard)

## Work the needs to be done

* Code for connecting the wallet
* Design and implementation of the database
* Poll creation design and code
* The capacity to 'vote,' sign a message verifying ownership of an address and it's balance
* Key provisioning validation ( simple rpc call? )
* Polling result solution
* Anything you think needs to be done - Let us know! 
